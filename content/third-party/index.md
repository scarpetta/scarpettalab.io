---
title: "Third-party libraries and assets"
---

The following libraries and assets are used in this site:

- [PhotoSwipe](https://github.com/dimsemenov/PhotoSwipe)
- [Source Sans](https://github.com/adobe-fonts/source-sans)

Please refer to the corresponding websites for copyright ans license information.
