---
title: "PDF Mix Tool"
---

![PDF Mix Tool logo](icon.svg)

## Table of contents

- [About](#about)
- [Installation](#installation)
- [Usage](#usage)
- [Contribute](#contribute)
- [Screenshots](#screenshots)

## About
PDF Mix Tool is a simple and lightweight application that allows you to perform common editing operations on PDF files.

{{< figure src="merge_files.png" caption="Screenshot of PDF Mix Tool" >}}

Base operations it can perform are the following:

- Merge two or more files specifying a page set for each of them
- Rotate pages
- Composite more pages onto a single one (N-up)
- Combinations of all of the above

Besides, it can also mix files alternating their pages, generate booklets, add white pages to a PDF file, delete pages from a PDF file, extract pages from a PDF file, edit the PDF document information.

It is written in C++ and depends only on Qt (version 5 or 6) and qpdf.

PDF Mix Tool is a free software distributed under the terms of the GNU GPLv3 license.

Other information about PDF Mix Tool can be found on the [GitLab page](https://gitlab.com/scarpetta/pdfmixtool) of the project.

## Installation

PDF Mix Tool can be installed from the official repositories of these distributions:

[![Packaging status](https://repology.org/badge/vertical-allrepos/pdfmixtool.svg)](https://repology.org/project/pdfmixtool/versions)

### Other methods

If your distribution is not in the list above, you can try one of the following methods.

#### Installing from source

You can download the source archives of each release [here](https://gitlab.com/scarpetta/pdfmixtool/tags).

To install PDF Mix Tool on a generic GNU/Linux system download the source archive of the latest release, decompress it and run the following commands inside the "pdfmixtool-x.x" directory:

```
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release
make
sudo make install
```

#### Install from Flathub or Snap Store

> WARNING: these packages could be out of date and/or not fully working.

PDF Mix Tool is also available on [Flathub](https://flathub.org/apps/details/eu.scarpetta.PDFMixTool) and on the [Snap Store](https://snapcraft.io/pdfmixtool).

## Usage

Select the operation you want to perform from the list on the left side of the application.

In the "Merge files" tab:

- Add PDF files clicking on the "Add PDF file" button
- Double-click on the entry of a file to edit its properties:
    - Set page filters using the format "1-5, 7-10, 15, 19" or "1-5 7-10 15 19". Leave the field blank to include the entire file. You can use overlapping intervals, pages will be replicated.
    - Set multipage profile for each file. You can use this option to compose pages (N-up) or to add margins to pages.
    - Set pages rotation for each file
    - Set the outline entry (table of contents entry) for each file
- You can add the same file more times
- Generate the output file clicking on the "Generate PDF" button and selecting the destination file

## Contribute

- [Submit a merge request](https://gitlab.com/scarpetta/pdfmixtool/merge_requests)
- [Report a bug](https://gitlab.com/scarpetta/pdfmixtool/issues)
- Help translating: [![Translations status](https://hosted.weblate.org/widgets/pdf-mix-tool/-/svg-badge.svg)](https://hosted.weblate.org/engage/pdf-mix-tool/?utm_source=widget)

## Screenshots

In this section you can find some screenshots of PDF Mix Tool while executing some common tasks.

{{< figure src="merge_files.png" caption="Merge 3 PDF files" >}}
{{< figure src="nup.png" caption="Page composition (N-up)" >}}
{{< figure src="rotate.png" caption="Rotate pages" >}}
{{< figure src="extract_pages.png" caption="Extract pages from a file" >}}
{{< figure src="delete_pages.png" caption="Delete pages from a file" >}}
{{< figure src="document_information.png" caption="Edit document information" >}}
