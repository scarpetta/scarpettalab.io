SRC = $(wildcard third-party/*.txt)
TAR = $(addprefix static/third-party/,$(basename $(notdir $(SRC))))

.PHONY: all clean

all: $(TAR)

static/third-party/%: third-party/%.txt
	$(eval URL := $(shell sed -n '1p' $<))
	$(eval SHA := $(shell sed -n '2p' $<))
	$(eval DIR := $(dir $@))
	mkdir -p $(DIR)
	curl $(URL) > $@
	echo "$(SHA) $@" | sha512sum -c

clean:
	rm -f $(TAR)
